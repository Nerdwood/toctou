'use strict';

// requirements
const express = require('express');
const bodyParser = require('body-parser');
const { TransferAmount } = require('./Transfer');

// constants
const PORT = process.env.PORT || 8080;

// main express program
const app = express();

// configurations
app.use(express.json());

/* **********************************
 *  -- NOTE --
 *
 * I tried (for way too long) to use "bodyParser" to handle requests,
 * but it only really works with POST requests (as they have a "body"). *facepalm*
 * GET requests have a limited input capability (as they should NOT have a body).
 * If this system requires POST requests in the future, bodyParser should be used to handle & limit the input instead of express.json().
 *
 *************************************/

// routes
// health check
app.get('/status', (req, res) => { res.status(200).end(); });
app.head('/status', (req, res) => { res.status(200).end(); });

/*
// Testing endpoint
app.get('/test', (req, res) => {
    try {

        // dataType, maxLength, minLength, lexicalCharset

        // Get parameters
        let testString = getParam(req, "testString", {
            dataType: 'string',
            maxLength: 20,
            minLength: 5,
            lexicalCharset: '-a-zA-Z0-9',
        });
        let testNumber = getParam(req, "testNumber", {
            dataType: 'number',
            lexicalCharset: '0-9',  // integer/digits only
        });
        let testBoolean = getParam(req, "testBoolean", {
            dataType: 'boolean',
        });

        console.log('testString', testString);
        console.log('testNumber', testNumber);
        console.log('testBoolean', testBoolean);

        // Success 200 OK
        res.status(200).end('Successful request to /test');

    } catch (ex) {
        res.status(400).end('Error with /test request: ' + ex);
    }
});
*/

// Send
app.get('/', (req, res) => {
    var balance = 1000;

    try {

        // Get amount parameter
        let amount = getParam(req, "amount", {
            dataType: 'number',
        });

        // Do transfer
        var { balance, transfered } = transfer(balance, amount);

        // Check if successful
        if (transfered !== undefined) {

            // Success 200 OK
            res.status(200).end('Successfully transfered: ' + transfered + '. Your balance: ' + balance);
        } else {

            // Error 400
            res.status(400).end('Insufficient funds. Your balance: ' + balance);
        }

    } catch (ex) {

        // Error 400
        res.status(400).end('Error processing transfer: ' + ex);
    }
});

// Transfer amount service
var transfer = (balance, amount) => {
    var transfered = 0;

    try {

        // Use custom Transfer value type
        let transferAmount = TransferAmount(amount);

        // Check if transfer amount is within balance
        if (transferAmount <= balance) {
            balance -= transferAmount;
            transfered += transferAmount;
            return { balance, transfered };
        // All other cases return an error
        } else {
            return { balance, undefined };
        }

    } catch (ex) {
        throw ex;
    }
};

/**
 * Get a parameter by its name, optionally passing through options.
 *
 * @param {object} req The request object to retrieve the parameter value from.
 * @param {string} paramName The name of the parameter to retrieve.
 * @param {object} options Configuration options: dataType, maxLength, minLength, lexicalCharset
 * @return {mixed} The parameter's value if valid, Error thrown otherwise.
 */
const getParam = (req, paramName, options = {dataType: 'string'}) => {

    // NOTE:
    // Can't use bodyParser to handle "dangerous" requests, as they're only GET requests...  :(
    // GET requests don't really have a body, so have limited input spamming capabilities.
    // If POST requests are needed in the future, bodyParser should be used to handle & limit input.
    // "req.body.param" instead of "req.query.param"

    let param;

    // Existence
    if (typeof req.query[paramName] === 'undefined') {
        throw new Error(`Parameter "${paramName}" doesn't exist.`);
    } else {
        param = req.query[paramName];
    }

    // Force type
    //   This could be done after performing the emptiness, normalisation and lexical checks
    //   (but the original parameter value is not needed in this case).
    switch (options.dataType || '') {  // Can't use ?? without Node 14  :(
        case 'string':
            param = String(param);
            break;
        case 'number':
            param = Number(param);
            break;
        case 'bigint':
            param = BigInt(param);
            break;
        case 'boolean':
            param = Boolean(param);
            break;
    }

    // Emptiness
    if (Number.isNaN(param)) {
        throw new Error(`Parameter "${paramName}" is not a valid number.`);
    } else if (null === param || undefined === param) {
        throw new Error(`Parameter "${paramName}" failed emptiness check.`);
    }

    // Decode - N/A for this context (done at specific value type level)

    // String-specific checks
    if (typeof param === 'string') {

        // Normalise characters
        param = param.normalize('NFC');

        // Input max length
        if (options.maxLength && param.length > options.maxLength) {
            throw new Error(`Parameter "${paramName}" is too long (max length = ${options.maxLength}).`);
        }

        // Input min length
        if (options.minLength && param.length < options.minLength) {
            throw new Error(`Parameter "${paramName}" is too short (min length = ${options.minLength}).`);
        }
    }

    // Lexical check
    if (options.lexicalCharset) {
        let lexicalRegex = new RegExp(`^[${String(options.lexicalCharset)}]*$`);
        if (!lexicalRegex.test(String(param))) {
            throw new Error(`Parameter "${paramName}" fails lexical check (chars = ${options.lexicalCharset}).`);
        }
    }

    // Syntax check - N/A for this context (done at specific value type level)
    // Semantic check - N/A for this context (done at specific value type level)

    return param;

};

// Fix to avoid EADDRINUSE during test
if (!module.parent) {
    // HTTP listener
    app.listen(PORT, err => {
        if (err) {
            console.log(err);
            process.exit(1);
        }
        console.log('Server is listening on port: '.concat(PORT));
    });
}
// CTRL+c to come to action
process.on('SIGINT', function() {
    process.exit();
});

module.exports = { app, transfer };
