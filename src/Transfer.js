function TransferAmount(amount) {

    // Define maximum transfer amount for all requests
    const maxTransferAmount = 1000000;

    // Force to primitive number value
    let numAmount = Number(amount);

    // Check if not number
    if (Number.isNaN(numAmount)) {
        throw new Error("Amount is not a valid number.");
    }

    // Fix to 4 decimal places
    numAmount = Number(numAmount.toFixed(4));

    // Check if out of range
    if (numAmount < 0 || numAmount > maxTransferAmount) {
        throw new Error(`Amount is out of range: 0-${maxTransferAmount}`);
    } else {
        return numAmount;
    }

    return 0;
}

module.exports = {TransferAmount};